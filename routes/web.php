<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//  Route::get('/', function () {
//    return view('/Home');
// });

Route::middleware('auth_token')->group(function () {
    Route::get('/', 'HomeController@getIndex');
    Route::get('home', 'HomeController@getIndex');
    Route::get('addEvent', 'addEvent@getIndex');

    Route::resource('article', 'ManagerArticleController');

    Route::get('articles', 'ManagerArticleController@show');

});
Route::get('login', ['as' => 'login', 'uses' => 'LoginController@getLogin']);
Route::post('login', 'LoginController@postLogin');
