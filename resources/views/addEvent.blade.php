@extends('master')
@section('main')
<div class="panel panel-primary" style="margin-top:50px">
    <div class="panel-body" >
        <legend><h2>Tạo Sự Kiện Mới</h2></legend>
        <form action="../api/XLDL/addActivity.php" method="POST" enctype="multipart/form-data">
            Tên Sự Kiện:
            <input class="form-control" name="nameAct" ><br/>

            Thời Gian Bắt Đầu:
            <div class="form-group col-12">
                <div class="input-group date form_datetime col-md-12" data-date="01-06-2019 10:00" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input class="form-control" name="timeStar" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
            Thời Gian Kết Thúc:
            <div class="form-group col-12">
                <div class="input-group date form_datetime col-md-12" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input class="form-control" size="16" name="timeEnd" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
            Thời Gian Bắt Đầu Điểm Danh:
            <div class="form-group col-12">
                <div class="input-group date form_datetime col-md-12" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input class="form-control" size="16" name="DeadlineStar"  type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
            Thời Gian Kết Thúc Điểm Danh:
            <div class="form-group col-12">
                <div class="input-group date form_datetime col-md-12" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                    <input class="form-control" name="DeadlineEnd" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
            Địa điểm:  <textarea class="form-control" name="Acplace" ></textarea><br/>
            Nội dung: <textarea class="form-control" rows='5' name="AcContent" ></textarea><br/>
            Hình ảnh Hoạt động<input type="file" id='image' name="image" onchange="readURL(this);"> <br />
                <img id="blah" height="150px" width="150px" src="http://placehold.it/150" alt="your image" /><br /> <br/>
            Điểm hoạt động:  <input type="number" class="form-control" name="ACPoint" ><br/>
            Số Lượng:  <input type="number" class="form-control" name="AcNum" ><br/>
            <button type="submit" class="btn btn-primary">Tạo hoạt động</button>
        </form>
    </div>
</div>
<script type="text/javascript" src="{{asset('bootstrap-datetimepicker-master/sample in bootstrap v3/jquery/jquery-1.8.3.min.js')}}" charset="UTF-8"></script>

<script type="text/javascript" src="{{asset('bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js')}}"></script>

<script type="text/javascript" src="{{asset('bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>

<script type="text/javascript" src="{{asset('bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js')}}" charset="UTF-8"></script>

<link href="{{asset('bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">

<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        format: 'yyyy-mm-dd hh:ii',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
</script>
@endsection

