@extends('master')
<!-- @section('main') -->
<!-- @if (session()->has('success'))
<script type="text/javascript">
alert({
    {
        session() - > get('success')
    }
});
</script>
@endif -->
<div class="panel-body">
    <div class="table-responsive">
        <legend>
            <h1>Xem tất cả bài viết</h1>
        </legend>
        <!-- <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>ManagerID</th>
                    <th>Image</th>
                    <th>Content</th>
                    <th>TimeUpLoad</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach($article as $articles)
                    <tr>
                        <td>{{$articles->id}}</td>
                        <td>{{$articles->Title}}</td>
                        <td>{{$articles->Description}}</td>
                        <td>{{$articles->ManagerID}}</td>
                        <td>
                            @if($articles->ImageUrl !=null)
                                <img width="100px" height="100px" src="{{asset('image/'.$articles->ImageUrl)}}" class="img-thumbnail" alt="Cinque Terre">
                            @else
                                <img width="100px" height="100px" src="{{asset('image/default_image.png')}}" class="img-thumbnail" alt="Cinque Terre">
                            @endif
                        </td>
                        <td>{{$articles->Content}}</td>
                        <td>{{$articles->TimeUpLoad}}</td>
                        <td>
                            <div class="row" style="padding-right:15%">

                                <a  href="{{ route('article.edit',$articles->id) }}"><button type="Button"  class="btn btn-primary col-xs-11">Sửa</button></a>
                                <form method="POST" action="{{ route('article.destroy',$articles->id)}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <div class="form-group">
                                        <input type="submit"  onclick="return confirm('Bạn có chắc chắn xóa?')" class="col-xs-11 btn btn-danger delete-article" value="Xóa">
                                    </div>
                                </form>
                                {{-- {{ Form::open(array('route'=>array('article.destroy',$articles->id),'method'=>'DELETE')) }}
                                    <input type="submit"  onclick="return confirm('Bạn có chắc chắn xóa?')" class="col-xs-11 btn btn-danger delete-article" value="Xóa">
                                    {{ csrf_field() }}
                                {{ Form::close() }} --}}
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$article->links()}} -->
    </div>
    <!-- /.table-responsive -->
</div>
<!-- @stop -->
