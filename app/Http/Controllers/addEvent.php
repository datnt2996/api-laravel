<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class addEvent extends Controller
{
    public function __construct()
    {
    }

    public function getIndex(Request $request)
    {
        $ManagerID = Cache::get('auth_user');
        $token = Cache::get($ManagerID);
        $request = ApiHelpers::request('http://tstudents.herokuapp.com', 'get', '/article?limit=50');
        $data = json_decode($request->getBody());
        return view('/addEvent')->with('data', $data->data);
    }
}