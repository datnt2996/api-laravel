<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\MessageBag;
use Validator;

class LoginController extends Controller
{

    public function getLogin(Request $request)
    {
        return view('/login');
    }
    public function postLogin(Request $request)
    {
        //dd($request->all());
        $rule = [
            'user' => 'required|email',
            'password' => 'required|min:6',
        ];
        $masage = [
            'user.required' => 'Tài khoản không được để trống',
            'user.email' => 'Bạn phải nhập địa chỉ email',
            'password.required' => 'Mật khẩu không được để trống',
            'password.min' => 'Bạn phải nhập ít nhất 6 ký tự',
        ];

        $validator = Validator::make($request->all(), $rule, $masage);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $user = $request->input('user');
            $password = $request->input('password');
            $json = ['json' => ['email' => $user, 'password' => $password]];
            $request = ApiHelpers::request('http://tstudents.herokuapp.com', 'post', '/auth/admin/token', $json);
            $resuilt = json_decode($request->getBody());
            if (!empty($resuilt->token)) {
                $resuilt->user = $user;
                // dd($resuilt);
                Cache::add('auth_user', $user);
                Cache::add($user, $resuilt->token);
                return redirect()->intended('home');
                // dd('Đăng nhập thành công!!!');
            } else {
                $errors = new MessageBag(['errorLogin' => 'Sai tên tài khoản hoặc mật khẩu!!!']);
                return redirect()->back()->withErrors($errors)->withInput();
            }
        }

    }
}