<?php

namespace App\Http\Controllers;

use App\article;
use App\Helpers\ApiHelpers;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Validator;

class ManagerArticleController extends Controller
{
    public function show($id, Request $request)
    {
        $ManagerID = Cache::get('auth_user');
        $request = ApiHelpers::request('http://tstudents.herokuapp.com', 'get', '/article?limit=20');
        $data = json_decode($request->getBody());
        return view('/managerarticles')->with('data', $data->data);
    }

    public function index(Request $request)
    {
        $ManagerID = Cache::get('auth_user');
        $request = ApiHelpers::request('http://tstudents.herokuapp.com', 'get', '/article?limit=20');
        $data = json_decode($request->getBody());
        return view('/managerarticles')->with('data', $data->data);
    }

    public function destroy($id)
    {
        $article = article::findOrFail($id);
        $image_path = storage_path("image/" . $article->ImageUrl); // Value is not URL but directory file path
        if (File::exists($image_path)) {
            File::delete($image_path);
            //echo "<script>alert('')</script>";
        }
        $article->delete();
        return redirect()->route('article.index')->with('success', 'Xóa Bài Viết Thành Công!!!');
    }

    public function store(Request $request)
    {
        $rule = [
            'Title' => 'required',
            'Description' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ];
        $masage = [
            'Title.required' => 'Tiêu đề không được để trống',
            'content.required' => 'Nội dung nhập ít nhất 4 ký tự',
            'Description.required' => 'Mô tả không được để trống',
            'image.required' => 'Hình ảnh không được để trống',
            'image.image' => 'Bạn phải chọn hình ảnh',
            'image.mimes' => 'Bạn phải đúng định dạnh hình ảnh:jpeg,png,jpg,gif,svg',
            'image.max' => 'File hình ảnh không được lớn hơn 10 Mb',
        ];
        $validator = Validator::make($request->all(), $rule, $masage);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            //save File
            $image = $request->file('image');
            $fileName = (Carbon::now()->format('Y-m-d_H-i-s')) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path("image"), $fileName);
            //save article
            $article = new article;
            $article->Title = $request->input('Title');
            $article->Description = $request->input('Description');
            $article->ManagerID = $request->session()->get('managerID');
            $article->imageUrl = $fileName;
            $article->Content = $request->input('content');
            $article->TimeUpLoad = Carbon::now()->toDateTimeString();
            $article->save();
            $request->session()->flash('alert', 'Tạo bài viết thành công!');
            return redirect()->route('article.index')->with('success', 'Tạo bài viết thành công');
        }
    }

    public function update(Request $request, article $article)
    {
        $rule = [
            'Title' => 'required',
            'Description' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        ];
        $masage = [
            'Title.required' => 'Tiêu đề không được để trống',
            'content.required' => 'Nội dung nhập ít nhất 4 ký tự',
            'Description.required' => 'Mô tả không được để trống',
            'image.required' => 'Hình ảnh không được để trống',
            'image.image' => 'Bạn phải chọn hình ảnh',
            'image.mimes' => 'Bạn phải đúng định dạnh hình ảnh:jpeg,png,jpg,gif,svg',
            'image.max' => 'File hình ảnh không được lớn hơn 10 Mb',
        ];
        $validator = Validator::make($request->all(), $rule, $masage);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            //save Image
            $image = $request->file('image');
            // $oldname = $request->image->getClientOriginalName();
            $fileName = $article->ImageUrl;
            //echo "<script>alert('".$fileName."')</script>";
            //echo "<script>alert('".$oldname."-".$article->imageUrl."')</script>";

            $image->move(public_path('image'), $fileName);

            // $new = [
            // 'Title' => $request->input('Title'),
            // 'Description' => $request->input('Description'),
            // 'ManagerID' => $request->session()->get('managerID'),
            // 'ImageUrl' => $fileName,
            // 'Content' => $request->input('content'),
            // 'TimeUpLoad' => Carbon::now()->toDateTimeString()];
            // $article->update($new);

            DB::table('articles')
                ->where('id', $article->id)
                ->update([
                    'Title' => $request->input('Title'),
                    'Description' => $request->input('Description'),
                    'ManagerID' => $request->session()->get('managerID'),
                    'ImageUrl' => $fileName,
                    'Content' => $request->input('content'),
                    'TimeUpLoad' => Carbon::now()->toDateTimeString()]);

            $request->session()->flash('alert', 'Sửa bài viết thành công!');
            return redirect()->route('article.index')->with('success', 'Sửa bài viết thành công');
        }
    }

    public function edit(article $article, Request $request)
    {
        if ($request->session()->has('managerID')) {
            return view('editArticle')->with('article', $article);
        } else {
            return view('login');
        }
    }
    public function create(Request $request)
    {
        return view('/addEvent');
    }
}