<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class BearerTokenAuth
{
    public function handle($request, Closure $next)
    {
        $token = Cache::get('auth_user');
        if (empty($token)) {
            return route('login');
        }
        return $next($request);
    }
}